/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;


namespace XploRe.Collections
{

    /// <inheritdoc cref="IEqualityComparer" />
    /// <summary>
    ///     Defines methods to support the comparison of objects for reference equality.
    /// </summary>
    [SuppressMessage("ReSharper", "PossibleInterfaceMemberAmbiguity")]
    public interface IReferenceEqualityComparer : IEqualityComparer<object>, IEqualityComparer
    {

    }

}
