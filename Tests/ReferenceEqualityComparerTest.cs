/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;


namespace XploRe.Collections.Tests
{

    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class ReferenceEqualityComparerTest
    {

        [Fact]
        public void Singleton()
        {
            ReferenceEqualityComparer.Instance.Should().BeSameAs(ReferenceEqualityComparer.Instance);
        }

        [Fact]
        [SuppressMessage("ReSharper", "UseObjectOrCollectionInitializer")]
        public void DictionaryUse()
        {
            // Contravariance of IEqualityComparer<T> for assignment.
            var dictionary = new Dictionary<object, string>(ReferenceEqualityComparer.Instance);

            // Must not throw an exception as both boxed integers are different objects.
            dictionary.Add(1, "A");
            dictionary.Add(1, "B");
        }


        #region Boxed Value Types

        [Fact]
        public void BoxedBooleanEqualityTest()
        {
            // Boxed values yield different instances.
            ReferenceEqualityComparer.Instance.Equals(true, true).Should().BeFalse();

            // Same instance equality.
            var instance = (object) true;
            ReferenceEqualityComparer.Instance.Equals(instance, instance).Should().BeTrue();
        }

        [Fact]
        public void BoxedInt32EqualityTest()
        {
            // Boxed values yield different instances.
            ReferenceEqualityComparer.Instance.Equals(1, 1).Should().BeFalse();

            // Same instance equality.
            var instance = (object) 1;
            ReferenceEqualityComparer.Instance.Equals(instance, instance).Should().BeTrue();
        }

        [Fact]
        public void BoxedBooleanHashCodeTest()
        {
            // Boxed values yield different instances.
            var hash1 = ReferenceEqualityComparer.Instance.GetHashCode(true);
            var hash2 = ReferenceEqualityComparer.Instance.GetHashCode(true);

            hash1.Should().NotBe(hash2);

            // Same instance equality.
            var instance = (object) true;
            hash1 = ReferenceEqualityComparer.Instance.GetHashCode(instance);
            hash2 = ReferenceEqualityComparer.Instance.GetHashCode(instance);

            hash1.Should().Be(hash2);
        }

        [Fact]
        public void BoxedInt32HashCodeTest()
        {
            // Boxed values yield different instances.
            var hash1 = ReferenceEqualityComparer.Instance.GetHashCode(1);
            var hash2 = ReferenceEqualityComparer.Instance.GetHashCode(1);

            hash1.Should().NotBe(hash2);

            // Same instance equality.
            var instance = (object) 1;
            hash1 = ReferenceEqualityComparer.Instance.GetHashCode(instance);
            hash2 = ReferenceEqualityComparer.Instance.GetHashCode(instance);

            hash1.Should().Be(hash2);
        }

        #endregion

    }

}
