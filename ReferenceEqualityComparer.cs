/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;


namespace XploRe.Collections
{

    /// <inheritdoc cref="IEqualityComparer" />
    /// <summary>
    ///     Equality comparer that ignores any underlying equality implementations and instead always performs reference
    ///     equality.
    /// </summary>
    [PublicAPI]
    public sealed class ReferenceEqualityComparer : IReferenceEqualityComparer
    {

        /// <summary>
        ///     The static reference equality comparer instance.
        /// </summary>
        /// <remarks>
        ///     Returns an <see cref="IReferenceEqualityComparer" /> to avoid issues due to the precedence of
        ///     <see cref="object.Equals(object,object)" /> on instances.
        /// </remarks>
        [NotNull]
        public static IReferenceEqualityComparer Instance { get; } = new ReferenceEqualityComparer();


        #region IEqualityComparer<object>

        /// <inheritdoc />
        [Pure]
        bool IEqualityComparer<object>.Equals([CanBeNull] object x, [CanBeNull] object y)
        {
            // Reference equality. Do not use ReferenceEquals() here, as we're already explicitly operating on objects
            // and ReferenceEquals() itself will also just use the same object equality operator.
            return x == y;
        }

        /// <inheritdoc />
        [Pure]
        int IEqualityComparer<object>.GetHashCode([CanBeNull] object obj)
        {
            // Use the basic internal object hash code implementation.
            return RuntimeHelpers.GetHashCode(obj);
        }

        #endregion


        #region IEqualityComparer

        /// <inheritdoc />
        [Pure]
        bool IEqualityComparer.Equals([CanBeNull] object x, [CanBeNull] object y)
        {
            // Reference equality. Do not use ReferenceEquals() here, as we're already explicitly operating on objects
            // and ReferenceEquals() itself will also just use the same object equality operator.
            return x == y;
        }

        /// <inheritdoc />
        [Pure]
        int IEqualityComparer.GetHashCode([CanBeNull] object obj)
        {
            // Use the basic internal object hash code implementation.
            return RuntimeHelpers.GetHashCode(obj);
        }

        #endregion

    }

}
