/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using XploRe.Runtime;


namespace XploRe.Collections
{

    /// <summary>
    ///     Represents a serial first-scheduled, first-executed runner of tasks.
    /// </summary>
    // TODO: Currently lacks all standard collection interfaces.
    public class TaskQueue
    {

        [NotNull]
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        [NotNull]
        // ReSharper disable once AssignNullToNotNullAttribute
        private Task _previousTask = Task.FromResult(true);

        [NotNull]
        private readonly object _synchronisationLock = new object();

        /// <summary>
        ///     Adds a new <see cref="Action" /> to the end of the queue.
        /// </summary>
        /// <param name="action">The <see cref="Action" /> instance to add to execution queue.</param>
        /// <returns><see cref="Task" /> instance for scheduled <paramref name="action" />.</returns>
        [NotNull]
        public Task Enqueue([NotNull] Action action)
        {
            if (action == null) {
                throw new ArgumentNullException(nameof(action));
            }

            lock (_synchronisationLock) {
                var scheduledTask = _previousTask.ContinueWith(
                    task => action(),
                    _cancellationTokenSource.Token,
                    TaskContinuationOptions.PreferFairness,
                    TaskScheduler.Default);

                if (scheduledTask == null) {
                    throw new RuntimeInconsistencyException(
                        $"{nameof(scheduledTask)} is null.",
                        "Failed to schedule action."
                    );
                }

                _previousTask = scheduledTask;

                return scheduledTask;
            }
        }

        /// <summary>
        ///     Adds a new <see cref="Func{T}" /> to the end of the queue.
        /// </summary>
        /// <param name="func">The <see cref="Func{T}" /> instnace to add to execution queue.</param>
        /// <returns><see cref="Task{TResult}" /> instance for scheduled <paramref name="func" />.</returns>
        [NotNull]
        public Task<T> Enqueue<T>([NotNull] Func<T> func)
        {
            if (func == null) {
                throw new ArgumentNullException(nameof(func));
            }

            lock (_synchronisationLock) {
                var scheduledTask = _previousTask.ContinueWith(
                    task => func(),
                    _cancellationTokenSource.Token,
                    TaskContinuationOptions.PreferFairness,
                    TaskScheduler.Default);

                if (scheduledTask == null) {
                    throw new RuntimeInconsistencyException(
                        $"{nameof(scheduledTask)} is null.",
                        "Failed to schedule function."
                    );
                }

                _previousTask = scheduledTask;

                return scheduledTask;
            }
        }

        /// <summary>
        ///     Determines whether the execution queue is currently empty.
        /// </summary>
        /// <returns><c>true</c> if execution queue is empty, otherwise <c>false</c>.</returns>
        public bool IsEmpty()
        {
            lock (_synchronisationLock) {
                return _previousTask.IsCompleted;
            }
        }

    }

}
